import speech_recognition as sr
from thaiaudio.database import Database
import os
import pickle

r = sr.Recognizer()
mic = sr.Microphone()

# Basic loop is:
#   - copy in

db = Database('facts.yaml')


if os.path.exists('session.cache'):
    with open('session.cache', 'rb') as f:
        (questions, seen) = pickle.load(f)

else:
    questions = db.get_x_facts( 20, 0.5 )
    seen = {}

def write_session():
    with open('session.cache', 'wb') as f:
        pickle.dump( (questions,seen), f, pickle.HIGHEST_PROTOCOL)

last_word = ""
last_word_attempts = 0

with mic as source:

    while len(questions) > 0:
        print (last_word)
        print (last_word_attempts)

        remain = len(questions)
        question = questions.pop(0)
        ( p, fact_id, english, thai ) = question

        is_seen = fact_id in seen
        seen[fact_id] = True

        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        print(f'Remain: {remain} ID: {fact_id} Seen: {is_seen} P: {p} Attempts: {last_word_attempts}')

        print("\n\n\nPlease say the Thai for: " + english)
        os.system("say \"How do you say: " + english + '?\"' )
        r.adjust_for_ambient_noise(source)
        print("(press enter when ready)")
        input()
        print("🔊")

        audio = r.listen(source, timeout = 3)
        print("Analysing ...")
        result = r.recognize_google(audio_data=audio, language = "th", show_all=True)

        try:
            best = result['alternative'][0]
            score = best['confidence']
            transcript = best['transcript']
        except:
            print("Erroneous transcription")
            transcript = "unknown"
            score = 0

        if ( transcript == thai ):
            print("Congratulations: " + str(score))
            if not is_seen:
                db.quiz( fact_id, True )
            os.system("say -v Kanya เก่งมาก")
            last_word = ""
            last_word_attempts = 0

        else:
            print ("WRONG: [" + transcript + "]")
            print ("RIGHT: [" + thai + "]")
            os.system("say -v Kanya You said " + transcript + " but I want " + thai + '. ' + thai + '. ' + thai + '!')

            if not is_seen:
                db.quiz( fact_id, False )

            if len(questions) > 0:
                end_of_list = questions[-1]
                if end_of_list[1] != fact_id:
                    questions.append(question)
                    print("We'll come back to that at the end too")

            if fact_id == last_word:
                #print("Another attempt")
                last_word_attempts = last_word_attempts + 1
            else:
                #print("Reattempt")
                last_word = fact_id
                last_word_attempts = 1

            if last_word_attempts < 2:
                #print("Fewer than four")
                questions.insert(0, question)
                print("Let's try that again")
            else:
                #print("Four or more")
                last_word = ""
                last_word_attempts = 0


        if not is_seen:
            db.write()

        input()
        write_session()