from yaml import safe_load, dump
import tempfile
import shutil
import uuid
import ebisu
from datetime import datetime

df = "%Y-%b-%d %H:%M:%S"

def toModel( dict ):
    return ( dict['alpha'], dict['beta'], dict['t'] )

def toDB( tuple ):
    (a,b,t) = tuple
    return { "alpha": float(a), "beta": float(b), "t": float(t) }

def since( dt ):
    now = datetime.now()
    date_time_obj = datetime.strptime(dt, df)
    delta = now - date_time_obj
    delta_seconds = delta.total_seconds()
    hours = delta_seconds / 3600
    return hours

class Database():
    def __init__(self, filename):
        self.filename = filename
        with open(filename, 'r') as stream:
            self.data = safe_load(stream)
            if self.data is None:
                self.data = {}

    def get_facts( self ):
        fact_buffer = []
        unused_facts = []

        for fact_id, fact in self.data.items():
            if fact['updated'] is None:
                unused_facts.append( (None,fact_id,fact['english'],fact['thai']) )
            else:
                model = toModel( fact['distribution'] )
                hours = since( fact['updated'] )
                prob = ebisu.predictRecall(model, hours, exact=True)
                fact_buffer.append( ( prob, fact_id, fact['english'],fact['thai'] ) )

        fact_buffer = sorted( fact_buffer, key=lambda tup: tup[0])
        print(fact_buffer)

    def get_x_facts( self, count, below ):
        fact_buffer = []
        unused_facts = []

        for fact_id, fact in self.data.items():
            if fact['updated'] is None:
                unused_facts.append( (None,fact_id,fact['english'],fact['thai']) )
            else:
                model = toModel( fact['distribution'] )
                hours = since( fact['updated'] )
                prob = ebisu.predictRecall(model, hours, exact=True)
                if prob < below:
                    fact_buffer.append( ( prob, fact_id, fact['english'],fact['thai'] ) )

        fact_buffer = sorted( fact_buffer, key=lambda tup: tup[0])
        print( "Total facts: %d" % len(fact_buffer) )
        fact_buffer = fact_buffer[:count]



        extras = count - len( fact_buffer )
        if extras > 0:
            unused_facts = unused_facts[:extras]
        else:
            unused_facts = []

        return fact_buffer + unused_facts


    def quiz( self, fact_id, value ):
        fact = self.data[fact_id]
        hours = 0.01

        if fact['updated'] != None:
            hours = since( fact['updated'] )

        model = toModel( fact['distribution'] )
        newModel = ebisu.updateRecall(model, 1.0 if value else 0.0, hours)
        fact['distribution'] = toDB( newModel )
        fact['updated'] = datetime.now().strftime(df)
        #print( fact_id + ' updated at ' + updated )

    def write( self ):
        fp = tempfile.NamedTemporaryFile()
        fp.write( dump(self.data, encoding='utf-8', allow_unicode=True ))
        fp.flush()
        shutil.copy( fp.name, self.filename )
        #print("Wrote out DB")

    def add( self, thai, english ):
        # First check we don't already have that
        for fact_id, fact in self.data.items():
            if fact['thai'] == thai:
                raise ValueError("Already have Thai %s" % thai)
            if fact['english'] == english:
                raise ValueError("Already have English %s" % english)

        dateTimeObj = datetime.now()
        now = dateTimeObj.strftime(df)

        # Package this up
        fact = {
            "english": english,
            "thai": thai,
            "added": now,
            "updated": None,
            "distribution": {
                "alpha": 3.0,
                "beta": 3.0,
                "t": 24
            }
        }
        fact_id = str(uuid.uuid1())

        self.data[fact_id] = fact